<?php
include("freelancer_session.php");
include("koneksi.php");

?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Direct Freelancer</title>
	
	<!--ini favicon-->
    <link rel="icon" type="image/png" href="img/px.png" />

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

   <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="#page-top">
        <img src="img/px.png">
      </a>
        <a class="navbar-brand js-scroll-trigger" href="index.html">Direct Freelancer</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Kategori</a>
            </li>
            <!--<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>-->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Team</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>


    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Kategori</h2>
            <h3 class="section-subheading text-muted">Pilih Kategori yang Kamu Inginkan</h3>
          </div>
        </div>
        <div class="row text-center">
      

          <!--untuk HTML-->
          <div class="col-md-4">
            <a href="submob1.php?id=1"><img src="img/icon_sub_kategori/l2.png" class="img-responsive img-circle" alt="Webite" width="154" height="148">
            </a>
            <h4 class="service-heading">HTML</h4>
          </div>

          <!--untuk CSS-->
          <div class="col-md-4">
            <a href="submob1.php?id=2"><img src="img/icon_sub_kategori/l7.png" class="img-responsive img-circle" alt="Database" width="160" height="152">
            </a>
              <h4 class="service-heading">CSS</h4>
          </div>

           <!--untuk PHP-->
          <div class="col-md-4">
            <a href="submob1.php?id=3"><img src="img/icon_sub_kategori/php.png" class="img-responsive img-circle" alt="Mobile Apps" width="164" height="156">
            </a>
              <h4 class="service-heading">PHP</h4>
          </div>

          <!--untuk XML-->
          <div class="col-md-4">
            <a href="submob1.php?id=4"><img src="img/icon_sub_kategori/xml-flat.png" class="img-responsive img-circle" alt="Webite" width="154" height="148">
            </a>
            <h4 class="service-heading">XML</h4>
          </div>

           <!--untuk JSON-->
          <div class="col-md-4">
            <a href="submob1.php?id=5"><img src="img/icon_sub_kategori/json-flat.png" class="img-responsive img-circle" alt="Webite" width="154" height="148">
            </a>
            <h4 class="service-heading">JSON</h4>
          </div>

           <!--untuk Javascript-->
          <div class="col-md-4">
            <a href="submob1.php?id=6"><img src="img/icon_sub_kategori/js-flat.png" class="img-responsive img-circle" alt="Webite" width="154" height="148">
            </a>
            <h4 class="service-heading">Javascript/h4>
          </div>


        </div>
      </div>
    </section>

        <!--Portfolio Grid
    <section class="bg-light" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Sub Kategori</h2>
            <h3 class="section-subheading text-muted"> Sub Kategori</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6 portfolio-item">
          <a href="subweb.php"><img src="img/icon_kategori/l5.png" class="img-responsive img-circle" alt="Webite" width="154" height="148">
            </a>
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/icon_sub_kategori/l2.png" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>HTML</h4>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/icon_sub_kategori/l7.png" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>CSS</h4>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/icon_sub_kategori/php.png" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>PHP</h4>
             
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/icon_sub_kategori/xml-flat.PNG" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>XML</h4>
              
            </div>
          </div>
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/icon_sub_kategori/json-flat.png" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>JSON</h4>
             
            </div>
          </div>
          
          <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/icon_sub_kategori/js-flat.png" alt="">
            </a>
            <div class="portfolio-caption">
              <h4>Javascript</h4>
              
            </div>
          </div>
        </div>
      </div>
    </section>-->

<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Direct Freelancer 2017</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <!--<li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>-->
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    
    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
<?php
$query = mysql_query("SELECT * FROM data_job ");
		while ($data=mysql_fetch_array($query)){
			$id_job = $data['id_job'];
            $kategori_job = $data['kategori_job'];
			$sub_kategori_job = $data['sub_kategori_job'];
            $desc_job = $data['desc_job'];
            $estimasi_job = $data['estimasi_job'];
            $nomor_hp = $data['nomor_hp'];
			$foto_job = $data['foto_job'];
			$email = $data['email'];
			//$fullname = $data['fullname'];
?>
                  <!-- Project Details Go Here -->
                  <h2><?php echo $desc_job;?></h2>
                  <img src="images/<?php echo $foto_job;?>" />
                  <p><?php echo $desc_job;?></p>
                  <ul class="list-inline">
                    <li>Date: <?php echo $estimasi_job;?></li>
                 
                    <li>Category: <?php echo $kategori_job;?></li>
					<li>Sub-Category: <?php echo $sub_kategori_job;?></li>
                    <li>HP: <?php echo $nomor_hp;?></li>
                    <li>Email : <?php echo $email;?></li>
                  </ul>
<?php } ?>
                </div>
              </div>
            </div>
          </div>

     <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
  </body>
</html>
