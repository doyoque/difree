<?php
include("freelancer_session.php");
include("koneksi.php");

$email = $_SESSION['email'];
$result=mysql_query("SELECT * FROM data_akun_freelancer WHERE email='$email'");
$row=mysql_fetch_row($result);
?>
<!DOCTYPE html>
<html >
<head>
	<meta charset="UTF-8">
	<title>Di Free Login</title>
	<!--ini favicon-->
	<link rel="icon" type="image/png" href="img/px.png" />
	<link rel="stylesheet" type="text/css" href="style.css">
	<style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,500);
		*:focus {
			outline: none;
		}

		.body {
			margin: 0;
			padding: 0;
			background: #DDD;
			font-size: 16px;
			color: #222;
			font-family: 'Roboto', sans-serif;
			font-weight: 300;
		}

		#edit-box {
			position: relative;
			margin: 5% auto;
			width: 500px;
			height: 550px;
			background: #FFF;
			border-radius: 2px;
			box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
		}

		.up-row {
			position: absolute;
			top: 0;
			left: 0;
			box-sizing: border-box;
			padding: 40px;
			width: 300px;
			height: 400px;
		}

		h1 {
			margin: 0 0 20px 0;
			font-weight: 300;
			font-size: 28px;
		}

		input[type="text"], input[type="password"] {
			display: block;
			box-sizing: border-box;
			margin-bottom: 20px;
			padding: 4px;
			width: 220px;
			height: 32px;
			border: none;
			border-bottom: 1px solid #AAA;
			font-family: 'Roboto', sans-serif;
			font-weight: 400;
			font-size: 15px;
			transition: 0.2s ease;
		}

		input[type="text"]:focus, input[type="password"]:focus {
			border-bottom: 2px solid #16a085;
			color: #16a085;
			transition: 0.2s ease;
		}

		input[type="submit"] {
			margin-top: 28px;
			width: 120px;
			height: 32px;
			background: #16a085;
			border: none;
			border-radius: 2px;
			color: #FFF;
			font-family: 'Roboto', sans-serif;
			font-weight: 500;
			text-transform: uppercase;
			transition: 0.1s ease;
			cursor: pointer;
		}

		input[type="submit"]:hover, input[type="submit"]:focus {
			opacity: 0.8;
			box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
			transition: 0.1s ease;
		}

		input[type="submit"]:active {
			opacity: 1;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
			transition: 0.1s ease;
		}
		
		.login-row {
			position: absolute;
			top: 400px;
			left: 38px;
			width: 220px;
			height: 40px;
			line-height: 10px;
			text-align: center;
		}

	</style>
	<script type="text/javascript" src="js/jquery.js"></script>
</head>


<body style="background-image:url(img/header-bg.jpg)">

	<div id="edit-box">
		<div class="up-row">
			<h1>Post Job</h1>
			<form method="post" action="freelancer_createjobP.php" enctype="multipart/form-data">
			<input type="hidden" name="email" value="<?php echo $email;?>">
			<table align="center" border="0" style="width: 100%">
                    <tr>
						<td>Category</td>
						<td>:</td>
						<td>
							<select name="kategori_job" style="width:220px;height:30px" required class="kategori_job">
								<option>--- Select Category ---</option>
								<option value="1">Website</option>
								<option value="2">Database</option>
								<option value="3">Mobile Apps</option>						
							</select>
						</td>
					</tr>
					<tr>
						<td>Sub-Category</td>
						<td>:</td>
						<td>
							<select name="sub_kategori_job" style="width:220px;height:30px" required class="sub_kategori_job">
								<option>--- Select Sub-Category ---</option>
								<option value="website">Mobile Engineer</option>
								<option value="database">Java Programmer</option>
								<option value="mobileapps">Software Engineer</option>						
							</select>
						</td>
					</tr>
					<tr>
						<td>Desc Job</td>
						<td>:</td>
						<td>
							<textarea rows="3" cols="26" name="desc_job" required></textarea>
						</td>
					</tr>
					<tr>
						<td>Estimated Time</td>
						<td>:</td>
						<td>
							<select name="estimasi_job" style="width:220px;height:30px" required="">
								<option>--- Select Estimated Time ---</option>
								<option value="3 days">3 days</option>
								<option value="1 week"> 1 week</option>						
							</select>
						</td>
					</tr>
					<tr>
						<td>Mobile Number Phone</td>
						<td>:</td>
						<td>
							<input type="text" name="nomor_hp" required />
						</td>
					</tr>
					<tr>
						<td>Background Image</td>
						<td>:</td>
						<td>
							<input type="file" name="photo_job" />
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="createjob_save" value="Add Job"/>
						</td>
					</tr>
			</table>
			</form>
		</div>
	</div>
<script type="text/javascript">
$(".kategori_job").change(function(){
	$(".sub_kategori_job").load("load_kategori_job.php?id_category="+$(this).val());
});
</script>
</body>
</html>
