<?php
include("freelancer_session.php");
include("koneksi.php");

$email = $_SESSION['email'];
$result=mysqli_query("SELECT * FROM data_akun_freelancer WHERE email='$email'");
$row=mysqli_fetch_row($result);
?>
<!DOCTYPE html>
<html >
<head>
	<meta charset="UTF-8">
	<title>Di Free Login</title>
	<!--ini favicon-->
	<link rel="icon" type="image/png" href="img/px.png" />
	<link rel="stylesheet" type="text/css" href="style.css">
	<style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,500);
		*:focus {
			outline: none;
		}

		.body {
			margin: 0;
			padding: 0;
			background: #DDD;
			font-size: 16px;
			color: #222;
			font-family: 'Roboto', sans-serif;
			font-weight: 300;
		}

		#edit-box {
			position: relative;
			margin: 5% auto;
			width: 500px;
			height: 550px;
			background: #FFF;
			border-radius: 2px;
			box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
		}

		.up-row {
			position: absolute;
			top: 0;
			left: 0;
			box-sizing: border-box;
			padding: 40px;
			width: 300px;
			height: 400px;
		}

		h1 {
			margin: 0 0 20px 0;
			font-weight: 300;
			font-size: 28px;
		}

		input[type="text"], input[type="password"] {
			display: block;
			box-sizing: border-box;
			margin-bottom: 20px;
			padding: 4px;
			width: 220px;
			height: 32px;
			border: none;
			border-bottom: 1px solid #AAA;
			font-family: 'Roboto', sans-serif;
			font-weight: 400;
			font-size: 15px;
			transition: 0.2s ease;
		}

		input[type="text"]:focus, input[type="password"]:focus {
			border-bottom: 2px solid #16a085;
			color: #16a085;
			transition: 0.2s ease;
		}

		input[type="submit"] {
			margin-top: 28px;
			width: 120px;
			height: 32px;
			background: #16a085;
			border: none;
			border-radius: 2px;
			color: #FFF;
			font-family: 'Roboto', sans-serif;
			font-weight: 500;
			text-transform: uppercase;
			transition: 0.1s ease;
			cursor: pointer;
		}

		input[type="submit"]:hover, input[type="submit"]:focus {
			opacity: 0.8;
			box-shadow: 0 2px 4px rgba(0, 0, 0, 0.4);
			transition: 0.1s ease;
		}

		input[type="submit"]:active {
			opacity: 1;
			box-shadow: 0 1px 2px rgba(0, 0, 0, 0.4);
			transition: 0.1s ease;
		}
		
		.login-row {
			position: absolute;
			top: 400px;
			left: 38px;
			width: 220px;
			height: 40px;
			line-height: 10px;
			text-align: center;
		}

	</style>
</head>


<body style="background-image:url(img/header-bg.jpg)">

	<div id="edit-box">
		<div class="up-row">
			<h1>Edit Profile</h1>
			<form method="post" action="freelancer_editprofileP.php" enctype="multipart/form-data">
			<input type="hidden" name="email" value="<?php echo $email;?>">
			
			<table align="center" border="0" style="width: 100%">
                    <tr>
						<td>Fullname</td>
						<td>:</td>
						<td>
							<input type="text" name="fname" value="<?php echo $row[4];?>" required />
						</td>
					</tr>
					<tr>
						<td>Biography</td>
						<td>:</td>
						<td>
							<textarea rows="3" cols="26" name="bio" required><?php echo $row[5];?></textarea>
						</td>
					</tr>
					<tr>
						<td>Birthday</td>
						<td>:</td>
						<td>
							<input type="date" name="birthday" value="<?php echo $row[6];?>" required >
						</td>
					</tr>
					<tr>
						<td>Location</td>
						<td>:</td>
						<td>
							<input type="text" name="location" value="<?php echo $row[7];?>" required />
						</td>
					</tr>
					<tr>
						<td>Photo</td>
						<td>:</td>
						<td>
							<input type="file" name="photo" value="images/<?php echo $row[8];?>" required />
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="editprofile_save" value="Save Changes"/>
						</td>
					</tr>
			</table>
			</form>
		</div>
	</div>
	
</body>
</html>
