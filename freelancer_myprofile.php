<?php
include("freelancer_session.php");
include("koneksi.php");

$email = $_SESSION['email'];    
//tampilkan tabel data_akun_freelancer
$result=mysqli_query($db,"SELECT * FROM data_akun_freelancer WHERE email='$email'");
$row=mysqli_fetch_row($result);
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Direct Freelancer</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	
	<!--ini favicon-->
    <link rel="icon" type="image/png" href="img/px.png" />

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
	
<style type="text/css">
		body{
    background: #eeeeee;
    font-family: 'Varela Round', sans-serif;
  }
    .form-inline {
        display: inline-block;
    }
  .navbar-header.col {
    padding: 0 !important;
  } 
  .navbar {
    color: #fff;
    background: #926dde;
    padding: 5px 16px;
    border-radius: 0;
    border: none;
    box-shadow: 0 0 4px rgba(0,0,0,.1);
  }
  .navbar img {
    border-radius: 50%;
    width: 36px;
    height: 36px;
    margin-right: 10px;
  }
  .navbar .navbar-brand {
    color: #efe5ff;
    padding-left: 0;
    padding-right: 50px;
    font-size: 24px;    
  }
  .navbar .navbar-brand:hover, .navbar .navbar-brand:focus {
    color: #fff;
  }
  .navbar .navbar-brand i {
    font-size: 25px;
    margin-right: 5px;
  }
  
  .navbar .nav-item i {
    font-size: 18px;
  }
  .navbar .nav-item span {
    position: relative;
    top: 3px;
  }
  .navbar .nav > li a {
    color: #efe5ff;
    padding: 8px 15px;
    font-size: 14px;    
  }
  .navbar .nav > li a:hover, .navbar .nav > li a:focus {
    color: #fff;
    text-shadow: 0 0 4px rgba(255,255,255,0.3);
  }
  .navbar .nav > li > a > i {
    display: block;
    text-align: center;
  }
  .navbar .dropdown-item i {
    font-size: 16px;
    min-width: 22px;
  }
    .navbar .dropdown-item .material-icons {
        font-size: 21px;
        line-height: 16px;
        vertical-align: middle;
        margin-top: -2px;
    }
  .navbar .nav-item.open > a, .navbar .nav-item.open > a:hover, .navbar .nav-item.open > a:focus {
    color: #fff;
    background: none !important;
  }
  .navbar .dropdown-menu {
    border-radius: 1px;
    border-color: #e5e5e5;
    box-shadow: 0 2px 8px rgba(0,0,0,.05);
  }
  .navbar .dropdown-menu li a {
    color: #777 !important;
    padding: 8px 20px;
    line-height: normal;
  }
  .navbar .dropdown-menu li a:hover, .navbar .dropdown-menu li a:focus {
    color: #333 !important;
    background: transparent !important;
  }
  .navbar .nav .active a, .navbar .nav .active a:hover, .navbar .nav .active a:focus {
    color: #fff;
    text-shadow: 0 0 4px rgba(255,255,255,0.2);
    background: transparent !important;
  }
  .navbar .nav .user-action {
    padding: 9px 15px;
  }
  .navbar .navbar-toggle {
    border-color: #fff;
  }
  .navbar .navbar-toggle .icon-bar {
    background: #fff;
  }
  .navbar .navbar-toggle:focus, .navbar .navbar-toggle:hover {
    background: transparent;
  }
  .navbar .navbar-nav .open .dropdown-menu {
    background: #faf7fd;
    border-radius: 1px;
    border-color: #faf7fd;
    box-shadow: 0 2px 8px rgba(0,0,0,.05);
  }
  .navbar .divider {
    background-color: #e9ecef !important;
  }
  @media (min-width: 1200px){
    .form-inline .input-group {
      width: 350px;
      margin-left: 30px;
    }
  }
  @media (max-width: 1199px){
    .navbar .nav > li > a > i {
      display: inline-block;      
      text-align: left;
      min-width: 30px;
      position: relative;
      top: 4px;
    }
    .navbar .navbar-collapse {
      border: none;
      box-shadow: none;
      padding: 0;
    }
    .navbar .navbar-form {
      border: none;     
      display: block;
      margin: 10px 0;
      padding: 0;
    }
    .navbar .navbar-nav {
      margin: 8px 0;
    }
    .navbar .navbar-toggle {
      margin-right: 0;
    }
    .input-group {
      width: 100%;
    }
  }
		
		.img-circle {
			width: 230px;
			height: 230px;
			border-radius: 50%;
			border:10px solid #ffffff;
		}
		
		.name-big {
			font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
			font-size: 30px;
		}
		
		.name-middle{
			font-size: 20px;
		}
		
</style>

  </head>

  <body id="page-top">
  
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="#page-top">
        <img src="img/px.png">
      </a>
        <a class="navbar-brand js-scroll-trigger" href="freelancer_home.php">Direct Freelancer</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
      
      <ul class=" nav navbar-nav navbar-right ml-auto">
        <li class="nav-item dropdown">
          <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle user-action"><?php echo $_SESSION['email'];?></a>
          <ul class="dropdown-menu">
            <li><a href="freelancer_myprofile.php" class="dropdown-item"><i class="fa fa-user-o"></i>Profile</a></li>
            <li class="divider dropdown-divider"></li>
            <li><a href="freelancer_logout.php" class="dropdown-item"><i class="material-icons"></i>Logout</a></li>
          </ul>
      </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
				<table align="center" border="0" style="width: 70%">
          <tr> 
            <td>
              <?php 
              if(empty($row[8])){
              ?>
              <img src="images/g.jpg" class="img-circle"/>
              <?php
              } else {
              ?>
              <img src="images/<?php echo $row[8];?>" class="img-circle"/>
              <?php
              } ?>
            </td>
          </tr>
					<tr><td class="name-big"><?php echo "$row[4]";?></td></tr>
          
					<tr>
						<td class="name-middle"><i class="material-icons" style="font-size:30px">work</i>&nbsp;<?php echo "$row[5]";?></td>
					</tr>
					<tr>
						<td class="name-middle"><i class="material-icons" style="font-size:30px">location_on</i><?php echo "$row[7]";?></td>
					</tr>
					<tr>
						<td><a href="freelancer_editprofile.php?email=<?php echo $row['1']?>"><button type="submit" class="btn btn-info" onClick="" style="width:110px;height:35px;">Edit Profile</button></a></td>
                    </tr>
					<tr>
						<td><a href="freelancer_createjob.php?email=<?php echo $row['1']?>"><button type="submit" class="btn btn-info" onClick="" style="width:180px;height:35px;">Create a new job</button></a></td>
                    </tr>
                </table>
        </div>
      </div>
    </header>

   

    <!-- Contact -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Contact Us</h2>
            <h3 class="section-subheading text-muted">Jika Ada Pertanyaan atau Kritik dan Saran</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Your Name *" required data-validation-required-message="Please enter your name.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="Your Email *" required data-validation-required-message="Please enter your email address.">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" required data-validation-required-message="Please enter your phone number.">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Your Message *" required data-validation-required-message="Please enter a message."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Send Message</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Direct Freelancer 2017</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <!--<li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>-->
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/01-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Threads</li>
                    <li>Category: Illustration</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/02-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Explore</li>
                    <li>Category: Graphic Design</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/03-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Finish</li>
                    <li>Category: Identity</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/04-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Lines</li>
                    <li>Category: Branding</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/05-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Southwest</li>
                    <li>Category: Website Design</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2>Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/06-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Window</li>
                    <li>Category: Photography</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>

  </body>

</html>
