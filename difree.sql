-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 16 Des 2017 pada 04.56
-- Versi Server: 5.5.27
-- Versi PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `difree`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'website'),
(2, 'database'),
(3, 'mobile');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_sub`
--

CREATE TABLE IF NOT EXISTS `category_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `category_sub`
--

INSERT INTO `category_sub` (`id`, `id_category`, `name`) VALUES
(1, 1, 'html'),
(2, 1, 'css'),
(3, 1, 'php'),
(4, 1, 'xml'),
(5, 1, 'json'),
(6, 1, 'javascript'),
(7, 2, 'DBA'),
(8, 2, 'system analyst'),
(9, 2, 'data analyst'),
(10, 3, 'mobile engineer'),
(11, 3, 'java programmer'),
(12, 3, 'software engineer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_akun_freelancer`
--

CREATE TABLE IF NOT EXISTS `data_akun_freelancer` (
  `username` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(10) NOT NULL,
  `password_retype` varchar(10) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `bio` varchar(50) NOT NULL,
  `birthday` varchar(10) NOT NULL,
  `location` varchar(50) NOT NULL,
  `photo` blob NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email` (`email`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_akun_freelancer`
--

INSERT INTO `data_akun_freelancer` (`username`, `email`, `password`, `password_retype`, `fullname`, `bio`, `birthday`, `location`, `photo`) VALUES
('', '', '', '', '', '', '', '', ''),
('amanda', 'amanda@gmail.com', 'amanda', 'amanda', 'Rachel Amanda Aurora', 'Data Analyst, youtuber', '1995-01-01', 'Indonesia', 0x72616368656c616d616e64612e6a7067),
('farahazh', 'farahazhaar@gmail.com', 'farah99', 'farah99', 'crealive', 'Creative Engineer', '1997-12-29', 'Bandung,ID', 0x435245414c495645204c4f474f2d30312e6a7067),
('ilham', 'ilham@gmail.com', 'ilham', 'ilham', '', '', '', '', ''),
('khoirunnis', 'khoi@gmail.com', 'khoii', 'khoii', 'Khoirunnisa', 'HTML here', '1996-12-20', 'Jakarta', 0x6b2e6a7067);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_job`
--

CREATE TABLE IF NOT EXISTS `data_job` (
  `id_job` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_job` varchar(50) NOT NULL,
  `sub_kategori_job` varchar(50) NOT NULL,
  `desc_job` varchar(50) NOT NULL,
  `estimasi_job` varchar(50) NOT NULL,
  `nomor_hp` varchar(50) NOT NULL,
  `foto_job` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  PRIMARY KEY (`id_job`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `data_job`
--

INSERT INTO `data_job` (`id_job`, `kategori_job`, `sub_kategori_job`, `desc_job`, `estimasi_job`, `nomor_hp`, `foto_job`, `email`) VALUES
(1, 'website', '10', 'I Will Do Programming of HTML', 'tiga', '081221001009', 'html01.jpg', 'amanda@gmail.com'),
(2, 'Database', '10', 'ngolah data ', '2 hari', '121212121212', 'lol.jpg', 'farah@gmail.com'),
(3, '2', '10', 'vvub', '3 days', '525262', '', ''),
(4, '3', '', 'hvhv', '1 week', '25151', '', ''),
(5, '1', '2', 'desc', '3 days', '34543435', 'Screenshot (1).png', ''),
(6, '1', '2', 'desc', '3 days', '34543435', 'Screenshot (1).png', ''),
(7, '2', '8', 'mobile sistem analis', '3 days', '084646613161', 'Screenshot (5).png', ''),
(8, '2', '8', 'ABC', '3 days', '082240118846', 'Screenshot (9).png', ''),
(9, '1', '1', 'pembuatan halam login', '3 days', '087666785477', 'html01.jpg', ''),
(10, '2', '8', 'sistem informasi', '3 days', '08223456789', 'CREALIVE LOGO-01.jpg', ''),
(11, '1', '1', 'membuat website', '1 week', '082115326445', 'CREALIVE LOGO-01.jpg', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `google_users`
--

CREATE TABLE IF NOT EXISTS `google_users` (
  `google_id` decimal(21,0) NOT NULL,
  `google_name` varchar(60) NOT NULL,
  `google_email` varchar(60) NOT NULL,
  `google_link` varchar(60) NOT NULL,
  `google_picture_link` varchar(60) NOT NULL,
  PRIMARY KEY (`google_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `google_users`
--

INSERT INTO `google_users` (`google_id`, `google_name`, `google_email`, `google_link`, `google_picture_link`) VALUES
(111014502709366769137, 'Khoirun Nisa', 'khoirunnisa.nisa1@gmail.com', 'https://plus.google.com/111014502709366769137', 'https://lh5.googleusercontent.com/-_9F0T1KOTx0/AAAAAAAAAAI/A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `signup`
--

CREATE TABLE IF NOT EXISTS `signup` (
  `username` varchar(25) NOT NULL,
  `e-mail` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `retype_pass` varchar(25) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `picture` tinyint(1) NOT NULL,
  `link` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Fuid` varchar(100) NOT NULL,
  `Ffname` varchar(60) NOT NULL,
  `Femail` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_fb`
--

CREATE TABLE IF NOT EXISTS `users_fb` (
  `UID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `Fuid` varchar(100) NOT NULL,
  `Ffname` varchar(60) NOT NULL,
  `Femail` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
