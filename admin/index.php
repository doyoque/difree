<?php
include("freelancer_session.php");
session_start();
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>AdminTelcomFest2018</title>
  
  <!--ini favicon-->
    <link rel="icon" type="image/png" href="img/px.png" />

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="../css/agency.min.css" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="#page-top">
        <img src="img/px.png">
      </a>
        <a class="navbar-brand js-scroll-trigger" href="#page-top">TelcomFest2018</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#page-top">Beranda</a>
            </li>
            <!--<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>-->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Informasi</a>
            </li>
            
           <li class="nav-item dropdown">
          <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle user-action"><?php echo $_SESSION['email'];?></a>
          <ul class="dropdown-menu">
            <li><a href="freelancer_myprofile.php" class="dropdown-item"><i class="fa fa-user-o"></i>Profile</a></li>
            <li class="divider dropdown-divider"></li>
            <li><a href="freelancer_logout.php" class="dropdown-item"><i class="material-icons"></i>Logout</a></li>
          </ul>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Masuk Sebagai, Penyelenggara</div>
          <div class="intro-heading">Welcome to Admin TelcomFest</div>
          <a class="btn btn-xl js-scroll-trigger" href="#services">Tell Me More</a>
        </div>
      </div>
    </header>
<!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Acara</h2>
            <h3 class="section-subheading text-muted">Berikut rangkaian acara kegiatan TelcomFest 2018</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4" >
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/icon_kategori/wdc.png" alt="">
         
            </div>
            </a>
            </span>
            <h4 class="service-heading">Web Design</h4>
            <p class="text-muted" > WEB Design TelcomFest 2018 merupakan kompetisi untuk para pengembang website di tingkat SMA/SMK sederajat. Melalui kompetisi ini, diharapkan dapat memberikan kontribusi dalam mencetak generasi baru pengembangan WEB.</p>
          </div>
          <div class="col-md-4">
             <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
           <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/icon_kategori/madc.png" alt="">
         
            </div>
            </a>
            </span>
            <h4 class="service-heading">Mobile Apps Development</h4>
            <p class="text-muted">Mobile Apps Development TelcomFest 2018 merupakan kompetisi membangun aplikasi mobile berbasis platform Android yang bertujuan memberikan kontribusi bagi generasi baru developer aplikasi mobile.</p>


          </div>
          <div class="col-md-4">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
              <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/icon_kategori/bg-img-icpc.png" alt="">
         
            </div>
          </a>
           <!--  <span class="fa-stack fa-4x">
              
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
            </a>
            </span> -->
            <h4 class="service-heading">International Collegiate Programming Contest</h4>
            <p class="text-muted">ICPC atau ACM-ICPC merupakan kompetisi pemrograman antar universitas yang diselenggarakan di bawah asuhan Association for Computing Machinery (ACM). Kompetisi ini terdiri dari tingkatan-tingkatan tertentu. Mulai dari wilayah dalam negara, antar negara regional, dan world final.</p>

          
          </div>

        </div>

      </div>
    </section>

<!-- 
    <section id="about1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">About</h2>
            <h3 class="section-subheading text-muted">Berikut rangkaian acara kegiatan TelcomFest 2018</h3>
          </div>
        </div>
        <div class="row text-center">
          
         TelcomFest (Tel-U Competition Festival) merupakan sebuah wadah bagi mahasiswa untuk ajang lomba dalam mempersiapkan perkembangan teknologi yang semakin canggih. TelcomFest sendiri dibentuk pada tahun 2018, oleh mahasiswa Sistem Informasi.

        </div>

      </div>
    </section> -->

    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Input Informasi Lomba </h2>
            <h3 class="section-subheading text-muted">-</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="Nama Lomba *" required data-validation-required-message="Harus Di Isi...">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="text" placeholder="Tanggal Lomba *" required data-validation-required-message="Harus Di Isi...">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" type="text" placeholder="Jenis Lomba *" required data-validation-required-message="Harus Di Isi...">
                    <p class="help-block text-danger"></p>
                  </div>
                  
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Deskripsi Lomba *" required data-validation-required-message="Harus Di Isi..."></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <button id="sendMessageButton" class="btn btn-xl" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; TelcomFest 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <!--<li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>-->
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

   

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/popper/popper.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="../js/jqBootstrapValidation.js"></script>
    <script src="../js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../js/agency.min.js"></script>

  </body>

</html>
