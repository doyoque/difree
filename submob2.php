<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Direct Freelancer</title>
  
  <!--ini favicon-->
    <link rel="icon" type="image/png" href="img/px.png" />

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
    <style>
    .about-section {
    height: 100%;
    padding-top: 70px;
    }

    .container {
    margin-left: 0;
    margin-right: 0;
  }
  /* Start Feature CSS */
.green-text { color: #178A27; }
.blue-text { color: #1686C1; }
.gray-text { color: #1686C1; }
.green-btn { background: #178A27; }
.green-btn:hover { background: #449D44; }
.blue-btn { background: #1686C1; }
.blue-btn:hover { background: #31B0D5; }
.red-btn { background: #852406; }
.red-btn:hover { background: #C9302C; }
.feature-content {
    background: #F2F2F2;
    padding-bottom: 40px;
}
.feature-content-title {
    padding: 0 20px;
    text-align: left;    
}
.feature-content-description {
    padding: 0 20px 35px;
    text-align: left;
    color: #333333;
    margin-top: 15px;
}
.feature-content-link {
    border-radius: 5px;
    color: #fff;
    font-size: 18px;
    padding: 10px 30px;
    text-decoration: none;
    text-transform: capitalize;
    -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
}
.feature-content-link:hover, .feature-content-link:link, .feature-content-link:visited  {
  color: white;
  text-decoration: none;
}

/* End Feature CSS */

    </style>
  </head>

  <body id="page-top">

   <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="#page-top">
        <img src="img/px.png">
      </a>
        <a class="navbar-brand js-scroll-trigger" href="index.html">Direct Freelancer</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Kategori</a>
            </li>
            <!--<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
            </li>-->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Tentang</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Team</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <section id="services">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Java Programmer</h2>
            <h4> Seluruh Java Programmer akan kamu temukan disini</h4>
          </div>
        </div>
        </section>
        <!-- Start Feature Area -->
    <section id="feature-area" class="about-section">
      <div class="container">
        <div class="row text-center inner">
          <div class="col-sm-4">
            <div class="feature-content">
              <img src="img/portfolio/mb1.jpeg" alt="" width="335" height="248">
              <h2 class="feature-content-title green-text">Mengubah websitemu ke Aplikasi Mobile (iOS, dan Android)</h2>
              <p class="feature-content-description">Apakah kamu memiliki suatu website yang mana 50% digunakan pada perangkat mobile?
              Saya akan membantu anda untuk mengubah website anda menjadi suatu platform yang tidak kalah responsifnya dengan website.
              </p>
              <ul class="list-inline">
                    <li>Date: Oktober 2017</li>
                    <li>Client: Julian</li>
                    <li>Category: aplikasi mobile</li>
                    <li>HP: 08112311341</li>
                    <li>Email : julian@gmail.com</li>
                  </ul>
                  <!--<a href="#" class="feature-content-link green-btn">button green</a>-->
            </div>
          </div>
          <div class="col-sm-4">
            <div class="feature-content">
              <img src="img/portfolio/mb2.jpg" alt="" width="335" height="248">
              <h2 class="feature-content-title blue-text">Peningkatan Performa Mobile Apps</h2>
              <p>Saya mempunyai passion pada mobile platform. Saya dapat menganalisa apa yang customer butuhkan pada aplikasi mobile yang telah berjalan. Meningkatkan layanan mobile yang sudah ada agar user merasa lebih nyaman saat menggunakan aplikasi.</p>
                  <ul class="list-inline">
                    <li>Date: Oktober 2017</li>
                    <li>Client: Rian</li>
                    <li>Category: programmer</li>
                    <li>HP: 08112331341</li>
                    <li>Email : rian@gmail.com</li>
                  </ul>                    
              <!--<a href="#" class="feature-content-link blue-btn">See Details</a>-->
            </div>
          </div>
          <div class="col-sm-4">
            <div class="feature-content">
              <img src="img/portfolio/mb3.png" alt="Image" width="335" height="248">
              <h2 class="feature-content-title red-text">Memperbaiki User Interface</h2>
              <p class="feature-content-description">Saya dapat memperbaiki tampilan UI yang akan dipesan.
              </p>
               <ul class="list-inline">
                    <li>Date: November 2017</li>
                    <li>Client: Nova</li>
                    <li>Category: programmer</li>
                    <li>HP: 08112314541</li>
                    <li>Email : vovro@gmail.com</li>
                  </ul>  
              <!--<a href="#" class="feature-content-link red-btn">Button Red</a>-->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Feature Area -->
          
        </div>
      </div>
    </section>

        

<!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; Direct Freelancer 2017</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="#">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="#">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>



     <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper/popper.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
  </body>
</html>